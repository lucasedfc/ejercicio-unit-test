const PaymentController = require("../../controllers/payment.controller");
const PaymentsService = require("../../services/payment.service");

jest.mock('../../services/payment.service');

beforeEach(() => {
    PaymentsService.getPayments.mockClear();
    PaymentsService.validate.mockClear();
    PaymentsService.create.mockClear();
})

describe("Payment Controller", () => {

    it("should return 100 when paymentGateway is naranja", () => {
        const paymentGateway = 'naranja';
        PaymentsService.getPayments.mockReturnValue([
            {
                payment_gateway: 'naranja',
                amount: 100
            },
            {
                payment_gateway: 'visa',
                amount: 500
            },
            {
                payment_gateway: 'visa',
                amount: 500
            }
        ]);
        const result = PaymentController.calculateTotal(paymentGateway);
        expect(result).toBe(100);
    });

    it("should return 500 when paymentGateway is visa", () => {
        const paymentGateway = 'visa';
        PaymentsService.getPayments.mockReturnValue([
            {
                payment_gateway: 'naranja',
                amount: 100
            },
            {
                payment_gateway: 'visa',
                amount: 500
            }
        ]);
        const result = PaymentController.calculateTotal(paymentGateway);
        expect(result).toBe(500);
    });

    it("should return 1000 when paymentGateway is visa and we have two payments of 500 with visa", () => {
        const paymentGateway = 'visa';
        PaymentsService.getPayments.mockReturnValue([
            {
                payment_gateway: 'naranja',
                amount: 100
            },
            {
                payment_gateway: 'visa',
                amount: 500
            },
            {
                payment_gateway: 'visa',
                amount: 500
            }
        ]);

        const result = PaymentController.calculateTotal(paymentGateway);
        expect(PaymentsService.getPayments).toHaveBeenCalledTimes(1);
        expect(result).toBe(1000);
    });

    it('should return the payments in asc mode', () => {

        PaymentsService.getPayments.mockReturnValue([
            {
                id: 1,
                amount: 7
            },
            {
                id: 2,
                amount: 2
            },
            {
                id: 3,
                amount: 21
            }
        ]);

        const result = PaymentController.sortPaymentsByAmount(true)
        const expectedResult = [
            {
                id: 2,
                amount: 2
            },
            {
                id: 1,
                amount: 7
            },
            {
                id: 3,
                amount: 21
            }
        ]

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return the payments in desc mode', () => {

        PaymentsService.getPayments.mockReturnValue([
            {
                id: 1,
                amount: 7
            },
            {
                id: 2,
                amount: 2
            },
            {
                id: 3,
                amount: 21
            }
        ]);

        const result = PaymentController.sortPaymentsByAmount(false)
        const expectedResult = [
            {
                id: 3,
                amount: 21
            },
            {
                id: 1,
                amount: 7
            },
            {
                id: 2,
                amount: 2
            },
        ]

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return an object grouped by payment gateway', () => {

        PaymentsService.getPayments.mockReturnValue([
            {
                id: 1,
                payment_gateway: 'visa'
            },
            {
                id: 2,
                payment_gateway: 'naranja'
            },
            {
                id: 3,
                payment_gateway: 'visa'
            },
            {
                id: 4,
                payment_gateway: 'naranja'
            }
        ]);

        const result = PaymentController.getPaymentGroupedByPaymentGateway();

        const expectedResult = {

            visa: [
                {
                    id: 1,
                    payment_gateway: 'visa'
                },
                {
                    id: 3,
                    payment_gateway: 'visa'
                }
            ],
            naranja: [
                {
                    id: 2,
                    payment_gateway: 'naranja'
                },
                {
                    id: 4,
                    payment_gateway: 'naranja'
                }
            ]
        };

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return a summary', () => {

        PaymentsService.getPayments.mockReturnValue([
            {
                amount: 1,
            },
            {
                amount: 3
            }
        ]);

        const result = PaymentController.getSummary();
        const expectedResult = {
            count: 2,
            total: 4
        }

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return an object filtered by id', () => {

        PaymentsService.getPayments.mockReturnValue([
            {
                id: 100,
                amount: 300,
                payment_gateway: "visa"
            },
            {
                id: 123,
                amount: 500,
                payment_gateway: "naranja"
            }
        ]);

        const result = PaymentController.getPaymentById(123);
        const expectedResult = {
            id: 123,
            amount: 500,
            payment_gateway: "naranja"
        };

        expect(result).toStrictEqual(expectedResult);

    });

    test('create payment', () => {

        PaymentController.createPayment({ id: 100 });
        expect(PaymentsService.validate).toHaveBeenCalledTimes(1);
        expect(PaymentsService.create).toHaveBeenCalledTimes(1);
        expect(PaymentsService.validate).toHaveBeenCalledWith({
            id: 100
        });
        expect(PaymentsService.create).toHaveBeenCalledWith({
            id: 100
        });
        
    });

    test('create payment exception', () => {
        const errMsg = 'validation failed';
        PaymentsService.validate.mockImplementation(() => {
            throw new Error(errMsg);
          });
        expect(() => PaymentController.createPayment({ id: 100})).toThrow(errMsg);
        expect(PaymentsService.validate).toHaveBeenCalledTimes(1);
        expect(PaymentsService.create).toHaveBeenCalledTimes(0);
    });


});
